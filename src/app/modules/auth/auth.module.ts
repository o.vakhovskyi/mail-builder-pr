import { MatButtonModule } from '@angular/material/button';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiComponentsModule } from 'src/app/components/ui-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatDialogModule } from '@angular/material/dialog';

import { LoginComponent } from './page/login/login.component';
import { FormComponent } from './components/form/form.component';
import { ResetPasswordDialogComponent } from './reset-password-dialog/reset-password-dialog.component';

@NgModule({
  declarations: [LoginComponent, FormComponent, ResetPasswordDialogComponent],
  imports: [
    CommonModule,
    UiComponentsModule,
    MatDialogModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: LoginComponent }]),
  ],
  exports: [RouterModule],
})
export class AuthModule { }
