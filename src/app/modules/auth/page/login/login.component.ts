import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { UserService } from 'src/app/services/user.service';
import { IUserData } from '../../../../repository/interfaces';
import { AuthService } from '../../auth.service';
import { ResetPasswordDialogComponent } from '../../reset-password-dialog/reset-password-dialog.component';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  userInfoSub!: Subscription;
  constructor(
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
    private snackbar: SnackBarService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void { }

  ngOnDestroy() {

    if (this.userInfoSub) {
      this.userInfoSub.unsubscribe();
    }
  }

  autorizationInfo(data: IUserData) {
    const formData = new FormData();
    formData.append('email', data.email);
    formData.append('password', data.password);

    this.userInfoSub = this.authService.login(formData).subscribe(
      (res) => {
        this.userService.setUserInfo(res.data),
          this.router.navigateByUrl('/projects');
      },
      (err) => {
        this.snackbar.openSnackBar({
          message: `Wrong Email or Password`,
          class: 'danger',
        });
      }
    );
  }

  openDialog() {

    const dialogRef = this.dialog.open(ResetPasswordDialogComponent);

    dialogRef.afterClosed().subscribe(email => {
      console.log('The dialog was closed', email);
      this.authService.resetPass({ email: email }).subscribe(
        data => console.log(data)

      )
    });

  }


}
