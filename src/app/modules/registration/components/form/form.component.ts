import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  Validators
} from '@angular/forms';
import { IUserRegisterData } from '../../../../repository/interfaces';
import { PATTERN_FOR_EMAIL } from 'src/app/app.constants';
import { ErrorStateMatcher } from '@angular/material/core';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
@Component({
  selector: 'mb-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  form!: FormGroup;
  matcher = new MyErrorStateMatcher();
  emailPlaceholder = 'Please enter your email';
  passwordPlaceholder = 'Please enter your password';
  password_confirmationPlaceholder = 'Confirm your password';
  inputTypePass = 'password';

  @Output() autorizationInfo: EventEmitter<IUserRegisterData> =
    new EventEmitter<IUserRegisterData>();
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      email: new FormControl('', [Validators.pattern(PATTERN_FOR_EMAIL)]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      password_confirmation: new FormControl('', [Validators.required, Validators.minLength(6)]),
    });
  }

  checkPass() {
    const password = this.form.get('password');
    const confirmPassword = this.form.get('password_confirmation');

    if (password?.value !== confirmPassword?.value && confirmPassword?.value.length >= 6) {
      this.form.get('password_confirmation')?.setErrors({ notSame: true });
    }
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    this.autorizationInfo.emit(this.form.value);
  }

}
