import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { UserService } from 'src/app/services/user.service';
import { IUserRegisterData } from '../../../../repository/interfaces';
import { RegistrationService } from '../../registration.service';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit, OnDestroy {
  userInfoSub!: Subscription;
  constructor(
    private registrationService: RegistrationService,
    private userService: UserService,
    private router: Router,
    private snackbar: SnackBarService
  ) { }

  ngOnInit(): void { }

  ngOnDestroy() {
    if (this.userInfoSub) {
      this.userInfoSub.unsubscribe();
    }
  }

  autorizationInfo(data: IUserRegisterData) {
    const formData = new FormData();
    formData.append('email', data.email);
    formData.append('password', data.password);
    formData.append('password_confirmation', data.password_confirmation);

    this.userInfoSub = this.registrationService.registration(formData).subscribe(
      (res) => {
        this.userService.setUserInfo(res.data),
          this.router.navigateByUrl('/projects');
      },
      (err) => {
        this.snackbar.openSnackBar({
          message: err.error.message.email ? err.error.message.email : err.error.message.password,
          class: 'danger',
        });
      }
    );
  }
}
