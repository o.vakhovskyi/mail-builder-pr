import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiComponentsModule } from 'src/app/components/ui-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { FormComponent } from './components/form/form.component';
import { RegistrationComponent } from './page/registration/registration.component';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  declarations: [RegistrationComponent, FormComponent],
  imports: [
    CommonModule,
    UiComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    RouterModule.forChild([{ path: '', component: RegistrationComponent }]),
  ],
  exports: [RouterModule],
})
export class RegistrationModule { }
