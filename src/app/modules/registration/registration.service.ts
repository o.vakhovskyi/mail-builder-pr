import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { IUser, IResponse } from 'src/app/repository/interfaces';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RegistrationService {
  constructor(private router: Router, private http: HttpClient) { }

  registration(data: FormData): Observable<IResponse<any>> {
    return this.http.post<IResponse<any>>(
      `${environment.apiUrl}/register`,
      data
    );
  }
}
