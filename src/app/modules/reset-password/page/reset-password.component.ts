import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { disableDebugTools } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { PATTERN_FOR_EMAIL } from 'src/app/app.constants';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { ResetPasswordService } from '../reset-password.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  form!: FormGroup;
  matcher = new MyErrorStateMatcher();
  emailPlaceholder = 'Please enter your email';
  passwordPlaceholder = 'Please enter your password';
  password_confirmationPlaceholder = 'Confirm your password';
  inputTypePass = 'password';
  token!: string;
  email!: string;

  constructor(
    private formBuilder: FormBuilder,
    private activateRoute: ActivatedRoute,
    private resetPassService: ResetPasswordService,
    private router: Router,
    private snackbar: SnackBarService,

  ) { }

  ngOnInit(): void {

    this.activateRoute.queryParams.subscribe(params => {
      this.token = params.token;
      this.email = params.email;
    }
    );

    this.form = this.formBuilder.group({
      email: new FormControl({ value: this.email, disabled: true }),
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      password_confirmation: new FormControl('', [Validators.required, Validators.minLength(6)]),
    });
  }

  checkPass() {
    const password = this.form.get('password');
    const confirmPassword = this.form.get('password_confirmation');

    if (password?.value !== confirmPassword?.value && confirmPassword?.value.length >= 6) {
      this.form.get('password_confirmation')?.setErrors({ notSame: true });
    }
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    const data = {
      email: this.email,
      token: this.token,
      password: this.form.value.password,
      password_confirmation: this.form.value.password_confirmation

    }

    this.resetPassService.resetPassword(data).subscribe(
      (res) => {
        this.router.navigateByUrl('/login');
      },
      (err) => {
        this.snackbar.openSnackBar({
          message: `${err.error.message}`,
          class: 'danger',
        });
      }
    )
  }
}
