import { MatFormFieldModule } from '@angular/material/form-field';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResetPasswordComponent } from './page/reset-password.component';
import { UiComponentsModule } from 'src/app/components/ui-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [ResetPasswordComponent],
  imports: [
    CommonModule,
    UiComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    RouterModule.forChild([{ path: '', component: ResetPasswordComponent }]),
  ],
  exports: [RouterModule]
})
export class ResetPasswordModule { }
