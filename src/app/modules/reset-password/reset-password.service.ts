import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IResponse } from 'src/app/repository/interfaces';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ResetPasswordService {

  constructor(private http: HttpClient) { }

  resetPassword(data: any): Observable<IResponse<any>> {
    console.log('resetPassword ', data);

    return this.http.post<IResponse<any>>(
      `${environment.apiUrl}/reset-password`,
      data
    );
  }
}
